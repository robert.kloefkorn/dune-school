#include <config.h>
/** \defgroup fv1 A finite-volume method for systems of conservation laws
    \ingroup FV
 **/

/** standard headers **/
#include <iostream>
#include <dune/common/version.hh>

/** dune (mpi, field-vector and grid type for dgf) **/
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>

typedef Dune::GridSelector::GridType Grid;

/** numerical scheme **/
#include "piecewisefunction.hh"

/** pde and problem description **/
#include "problem.hh"
typedef InitialModel< Grid::dimensionworld > ModelType;

// method
// ------
void method ( const ModelType &model, int startLevel )
{
  /* Grid construction ... */
  std::string name = model.problem().gridFile( "../data" );
  Dune::GridPtr<Grid> gridPtr(name);

  Grid &grid = *gridPtr;
  grid.loadBalance();

  /* ... some global refinement steps */
  std::cout << "globalRefine: " << startLevel << std::endl;

  /** type of level grid view **/
  typedef Grid::LeafGridView GridView;
  GridView gridView = grid.leafGridView();
  /****************************************************************
   *** TODO: do some initial refinement                         ***
   ***       and compute volume of domain                       ***
   ****************************************************************/

  /* construct data vector for solution */
  typedef PiecewiseFunction< GridView, Dune::FieldVector< double, ModelType::dimRange > > DataType;
  DataType solution( gridView );
  /* initialize data */
  solution.initialize( model.problem() );

  /* create VTK writer for data sequqnce */
  Dune::VTKSequenceWriter< GridView > vtkOut( gridView, "solution", "../output", ".", Dune::VTK::nonconforming );
  VTKData< DataType >::addTo( solution, vtkOut );

  /* output the initial grid and the solution */
  vtkOut.write( 0.0 );

}
/***************************************************
 ** main program with parameters:                 **
 ** 1) number of problem to use (initial data...) **
 ** 2) number of global refinement steps          **
 ** 3) maximal level to use during refinement     **
 ***************************************************/
int main ( int argc , char **argv )
try
{
  /* initialize MPI, finalize is done automatically on exit */
  Dune::MPIHelper &mpi = Dune::MPIHelper::instance( argc, argv );

  if( argc < 2 )
  {
    /* display usage */
    if( mpi.rank() == 0 )
      std::cout << "Usage: " << argv[ 0 ] << " [problem-nr] [startLevel]" << std::endl;
    return 0;
  }
  /* create problem */
  ModelType model(atoi(argv[1]));

  /* get level to use for computationa */
  const int startLevel = (argc > 2 ? atoi( argv[ 2 ] ) : 0);
  method( model, startLevel );

  /* done */
  return 0;
}
catch( const std::exception &e )
{
  std::cout << "STL ERROR: " << e.what() << std::endl;
  return 1;
}
#if ! DUNE_VERSION_NEWER(DUNE_COMMON, 2, 5)
catch( const Dune::Exception &e )
{
  std::cout << "DUNE ERROR: " << e << std::endl;
  return 1;
}
#endif
catch( ... )
{
  std::cout << "Unknown ERROR" << std::endl;
  return 1;
}
