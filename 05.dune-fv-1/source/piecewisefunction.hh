#ifndef PIECEWISEFUNCTION_HH
#define PIECEWISEFUNCTION_HH

#include <vector>
#include <fstream>

#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/grid/common/datahandleif.hh>

// PiecewiseFunction
// ----------
/** \class PiecewiseFunction
 *  \brief a piecewise constant function
 *
 *  \tparam  View   grid view on which the function is defined
 *  \tparam  Range  type for the range vector of the function
 *                  This class must have a vector like structure, i.e.,
 *                  a double operator[](int) method.
 *                  Furthermore we expect an axpy method on the class.
 */
template< class View, class Range >
class PiecewiseFunction
{
  typedef PiecewiseFunction< View, Range > This;

public:
  typedef View GridView;
  typedef Range RangeType;

  static const unsigned int order = 0;

  typedef std::vector< RangeType > VectorType;

  typedef typename GridView::template Codim< 0 >::Entity Entity;
  typedef typename GridView::template Codim< 0 >::Geometry Geometry;

public:
  /** \brief constructor
   *
   *  \param[in]  gridView  grid view the function lives on
   */
  PiecewiseFunction( const GridView &gridView )
  : gridView_( gridView ),
    dof_( gridView.indexSet().size( 0 ) )
  {}

  const RangeType &operator[] ( const size_t &index ) const
  {
    return dof_[ index ];
  }

  RangeType &operator[] ( const size_t &index )
  {
    return dof_[ index ];
  }

  /** \brief access the DoFs on one entity (of codimension 0)
   *
   *  \param[in]  entity  entity whose DoFs to access
   *
   *  \returns a reference to the DoFs of the entity
   */
  const RangeType &operator[] ( const Entity &entity ) const;

  /** \brief access the DoFs on one entity (of codimension 0)
   *
   *  \param[in]  entity  entity whose DoFs to access
   *
   *  \returns a reference to the DoFs of the entity
   */
  RangeType &operator[] ( const Entity &entity );

  /** \brief obtain the size of the dof vector;
             might be larger that number of elements in grid view. */
  size_t size() const
  {
    return dof_.size();
  }

  /** \brief obtain the grid view, this function lives on */
  const GridView &gridView () const
  {
    return gridView_;
  }

  /** \brief method to initialize data
   *  \param problemData instance of a class having a method initial taking
   *         points \c x in the global coordinate system
   */
  template <class ProblemData>
  void initialize ( const ProblemData &problemData );

private:
  GridView gridView_;
  /* storage for dofs */
  VectorType dof_;
};

  /****************************************************************
   *** TODO: Implement methods on Data class                    ***
   ****************************************************************/

template< class View, class Range >
inline const typename PiecewiseFunction< View, Range >::RangeType &
PiecewiseFunction< View, Range >::operator[] ( const Entity &entity ) const
{
  /**** TODO ****/
}

template< class View, class Range >
inline typename PiecewiseFunction< View, Range >::RangeType &
PiecewiseFunction< View, Range >::operator[] ( const Entity &entity )
{
  /**** TODO ****/
}

template< class View, class Range >
template< class ProblemData >
inline void
PiecewiseFunction< View, Range >::initialize ( const ProblemData &problemData )
{
  /**** TODO: replace the following by a grid traversal ...
   *          ... and initialize data using problemData.initial( baryCenter )
   *****/
  int rank = gridView().grid().comm().rank();
  std::fill( dof_.begin(), dof_.end(), rank );
}

template< class Data >
struct VTKData;

/**
 * \brief a class for vtk output of a PiecewiseFunction instance
 */
template< class GridView, int dimRange >
struct VTKData< PiecewiseFunction< GridView, Dune::FieldVector<double,dimRange> > >
: public Dune::VTKWriter< GridView >::VTKFunction
{
  typedef PiecewiseFunction< GridView, Dune::FieldVector<double,dimRange> > Data;
  typedef VTKData< Data > This;

  static const int dim = GridView::dimension;
  typedef typename GridView::template Codim< 0 >::Entity Entity;
  typedef Dune::FieldVector< double, dim > DomainType;

  //! number of components for scalar use 1 and for vector 2 or 3
  int ncomps () const
  {
    return 1;
  }

  //! evaluate function (comp<ncomps) on entity for local coordinate xi
  double evaluate ( int comp, const Entity &e, const DomainType &xi ) const
  {
    int index = data_.gridView().indexSet().index(e);
    return data_[ index ][ comp_ ];
  }

  //! name for this function
  std::string name () const
  {
    std::stringstream ret;
    ret << name_ << "_" << comp_;
    return ret.str();
  }

  //! add a PiecewiseFunction to the VTKWriter
  static void addTo ( const Data &data, Dune::VTKSequenceWriter< GridView > &vtkWriter )
  {
    /* the vtk-Writer class takes ownership of the function added - VTKData
     * is merely a wrapper for the Data class */
    for( int i = 0; i < dimRange; ++i )
    {
      std::shared_ptr< This > ptr( new This( data, i, "data" ) );
      vtkWriter.addCellData( ptr );
    }
  }
  //! add a PiecewiseFunction to the VTKWriter
  static void addTo ( const Data &data, const std::string &name, Dune::VTKSequenceWriter< GridView > &vtkWriter )
  {
    /* the vtk-Writer class takes ownership of the function added - VTKData
     * is merely a wrapper for the Data class */
    for( int i = 0; i < dimRange; ++i )
    {
      std::shared_ptr< This > ptr( new This( data, i, name ) );
      vtkWriter.addCellData( ptr );
    }
  }

private:
  VTKData ( const Data &data, unsigned int comp, const std::string &name )
  : data_( data ),
    comp_( comp ),
    name_(name)
  {}
  const Data &data_;
  unsigned int comp_;
  const std::string name_;
};

#endif // #ifndef PIECEWISEFUNCTION_HH
