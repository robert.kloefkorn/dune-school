import ufl
from ufl import grad, div, jump, avg, dot, dx, ds, dS, inner, sin, cos, pi, exp, sqrt
import dune.ufl
import dune.grid
import dune.fem
import dune.generator
import dune.alugrid

useAdaptivity = True

endTime  = 100
saveInterval = 0.5 # for VTK
initialRefinements = 8
globalTolerance = 5e-1
refineFraction = 0.9
coarsenFraction = 0.4

domain = dune.grid.cartesianDomain([-1,-1],[1,1],[1,1])
gridView = dune.fem.view.adaptiveLeafGridView(dune.alugrid.aluConformGrid(domain))
gridView.hierarchicalGrid.globalRefine(initialRefinements)

space = dune.fem.space.lagrange(gridView, order=1, storage="istl")
u     = ufl.TrialFunction(space)
phi   = ufl.TestFunction(space)
x     = ufl.SpatialCoordinate(space)
dt    = dune.ufl.Constant(0.1, "timeStep")
t     = dune.ufl.Constant(0.0, "time")

# define storage for discrete solutions
uh     = space.interpolate(0, name="uh")
uh_old = uh.copy()

# initial solution
initial = 0

# problem definition

# moving oven
ROven = 0.6
omegaOven = 0.05*2*pi
P = lambda s: ufl.as_vector([ROven*cos(omegaOven*s), ROven*sin(omegaOven*s)])
rOven = 0.2
ovenEnergy = 8
chiOven = lambda s: ufl.conditional(dot(x-P(s), x-P(s)) < rOven**2, 1, 0)
ovenLoad = lambda s: ovenEnergy * chiOven(s)

# desk in corner of room
deskCenter = [-0.8, -0.8]
deskSize = 0.2
chiDesk = ufl.conditional(abs(x[0]-deskCenter[0]) < deskSize, 1, 0)\
  * ufl.conditional(abs(x[1] - deskCenter[1]) < deskSize, 1, 0)

# Robin condition for window
windowWidth = 0.5
transmissionCoefficient = 1.2
outerTemperature = -5.0
chiWindow = ufl.conditional(abs(x[1]-1.0) < 1e-8, 1, 0)*ufl.conditional(abs(x[0]) < windowWidth, 1, 0)
rBC = transmissionCoefficient * (u - outerTemperature) * chiWindow

# heat diffussion
K = 0.01

# space form
diffusiveFlux = K*grad(u)
source = -ovenLoad(t+dt)

xForm = dot(diffusiveFlux, grad(phi)) * dx + source * phi * dx + rBC * phi * ds

# add time discretization
form = dot(u - uh_old, phi) * dx + dt * xForm

scheme = dune.fem.scheme.galerkin(form == 0, solver="cg",\
  parameters={"newton.linear.preconditioning.method":"ilu"})

# define a residual error estimator

# helper constructs for the estimator
elementStorage = dune.fem.space.finiteVolume(gridView)
estimate = elementStorage.interpolate(0, name="estimate")

chiT = ufl.TestFunction(elementStorage)
hT = ufl.MaxCellEdgeLength(elementStorage)
he = ufl.MaxFacetEdgeLength(elementStorage)
n  = ufl.FacetNormal(elementStorage)

# write down a residual estimator as UFL form
residual = (u-uh_old)/dt - div(diffusiveFlux) + source
estimatorForm = hT**2 * residual**2 * chiT * dx\
  + he * (inner(diffusiveFlux, n) - rBC)**2 * chiT * ds\
  + he * inner(jump(diffusiveFlux), n('+'))**2 * avg(chiT) * dS
estimator = dune.fem.operator.galerkin(estimatorForm)

# prepare for the time loop: initial data and "plot" the initial solution

nextSaveTime = saveInterval

uh.interpolate(initial)

vtk = gridView.sequencedVTK("heatrobin", pointdata=[uh,estimate])
vtk()

while t.value < endTime:
    uh_old.assign(uh)
    info = scheme.solve(target=uh)
    deskTemperature = dune.fem.function.integrate(gridView, uh * chiDesk, order=1) / deskSize**2 / 4
    estimator(uh, estimate)

    errorEstimate = sum(estimate.dofVector)
    print("estimated error: ", sqrt(errorEstimate))
    print("min. est.^2: ", min(estimate.dofVector))
    print("max. est.^2: ", max(estimate.dofVector))

    t.value += dt.value
    print("Computed solution at time", t.value,
              "desk temperature", deskTemperature,
              "iterations: ", info["linear_iterations"],
              "#Ent: ", gridView.size(0) )
    if t.value >= nextSaveTime or t.value >= endTime:
        vtk()
        nextSaveTime += saveInterval

    if useAdaptivity:
        avgTolerance = globalTolerance**2 / gridView.size(0)
        refineTol = avgTolerance * refineFraction
        coarsenTol = avgTolerance * coarsenFraction
        print("local tolerances ", avgTolerance, refineTol, coarsenTol)
        [refined, coarsened] = dune.fem.mark(estimate, refineTol, coarsenTol)
        print("#refined/coarsended: ", refined, coarsened)
        dune.fem.adapt([uh])
