/** always start a dune main file by including config.hh **/
#include <config.h>

/** standard headers **/
#include<iostream>

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
#include "linear.hh"
/*********************************************************/

int main ()
{

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  {
    typedef typename Dune::LinearMapping::GlobalCoordinate GlobalCoordinate;
    typedef typename Dune::LinearMapping::GlobalCoordinate LocalCoordinate;

    GlobalCoordinate p0( {{ -0.7, -1, 8 }} );
    GlobalCoordinate p1( {{ -1.2, -1, 8 }} );
    GlobalCoordinate p2( {{ -0.494975, -1.49497, 7.375 }} );
    GlobalCoordinate p3( {{ -0.7, -1, 8.5 }} );

    // reference tetrahedron
    /*
    GlobalCoordinate p0( {{ 0, 0, 0 }} );
    GlobalCoordinate p1( {{ 1, 0, 0 }} );
    GlobalCoordinate p2( {{ 0, 1, 0 }} );
    GlobalCoordinate p3( {{ 0, 0, 1 }} );
    */

    Dune::LinearMapping map( p0, p1, p2, p3 );

    LocalCoordinate local( 0.25 );
    auto center = map.global( local );

    std::cout << center << std::endl;

    auto localCenter = map.local( center );
    std::cout << localCenter << std::endl;
  }
/*********************************************************/

  return 0;
}
