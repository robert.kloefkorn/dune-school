#include <dune/geometry/quadraturerules.hh>
template< class GridView, class GF >
double integrate ( const GridView &gridView, const GF &gf, int order )
/* From the Python example
value = 0
lf = f.localFunction()
for e in view.elements:
    lf.bind(e)
    geo = e.geometry
    for p in quadratureRule(e.type, 4):
        x = p.position
        w = p.weight * geo.integrationElement(x)
        value +=  lf(x) * w
    lf.unbind()
*/
{
  typedef Dune::QuadratureRules< double, GridView::dimension > Rules;
  double value = 0;
  auto lf = localFunction( gf );
  for( const auto &entity : elements( gridView ) )
  {
    lf.bind( entity );
    const auto geo = entity.geometry();
    for( const auto &p : Rules::rule( entity.type(), order ) )
    {
      auto x = p.position();
      double weight = p.weight() * geo.integrationElement( x );
      value += lf( x ) * weight;
    }
    lf.unbind();
  }
  return value;
}

/* A vectorized version of the integration:
 * the callback into Python is done once on each element and the function
 * is evaluate for all quadrature points in one go.
 */
// to provide vectorized callback into python
#include <dune/python/geometry/quadraturerules.hh>
template< class GridView, class GF >
double integrateVectorized ( const GridView &gridView, const GF &gf, int order )
{
  typedef Dune::QuadratureRules< double, GridView::dimension > Rules;
  double value = 0;
  auto lf = localFunction( gf );
  for( const auto &entity : elements( gridView ) )
  {
    lf.bind( entity );
    const auto geo = entity.geometry();
    // make vectorized call to Python GridFunction possible
    const auto &rule = Rules::rule( entity.type(), order );
    auto pointsWeights = Dune::Python::quadratureToNumpy( rule );
    // here we need parts of pybind11's wrappers for numpy array
    const auto &valuesArray = lf( pointsWeights.first ).template cast< pybind11::array_t< double > >();
    const auto &values = valuesArray.template unchecked< 1 >();
    // now we compute the actual quadrature:
    for( std::size_t p = 0; p < rule.size(); ++p )
    {
      const auto &qp = rule[p];
      const auto &x = qp.position();
      double weight = qp.weight() * geo.integrationElement( x );
      value += values[ p ] * weight;
    }
    lf.unbind();
  }
  return value;
}
