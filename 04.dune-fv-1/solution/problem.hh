#ifndef PROBLEM_HH
#define PROBLEM_HH

#include <sstream>

#include <dune/common/fvector.hh>

/** \class ProblemData
 *  \brief virtual base class for our problems
 *
 *  \tparam  dimD  dimension of the domain
 *  \tparam  dimR  dimension of the range
 */
template< int dimD, int dimR >
struct ProblemData
{
  // dimension of domain and range
  static const int dimDomain = dimD;
  static const int dimRange  = dimR;

  // type of domain and range vectors
  typedef double DomainFieldType;
  typedef double RangeFieldType;
  typedef Dune::FieldVector< DomainFieldType , dimDomain > DomainType;
  typedef Dune::FieldVector< RangeFieldType  , dimRange >  RangeType;

  /** \brief virtual destructor */
  virtual ~ProblemData() {}

  /** \brief obtain the file name of the macro grid for this problem
   *
   *  \param[in]  path  path to the macro grids
   *
   *  \returns the file name of the macro grid
   */
  virtual std::string gridFile ( const std::string &path ) const = 0;

  /** \brief evaluate the initial data
   *
   *  \param[in]  x  coordinate to evaluate the initial data in
   *
   *  \returns the evaluated initial data
   */
  virtual RangeType initial ( const DomainType &x ) const = 0;

}; // end class ProblemData
/** \class SinData
 *  \brief Implementation of the virtual base class Data for sinusoidal
 *  functiion. The dimension of the range is 1 (scalar problem).
 *
 *  \tparam  dimD  dimension of the domain
 */
template< int dimD >
class SinData
: public ProblemData< dimD, 1 >
{
  typedef ProblemData< dimD, 1 > Base;

public:
  static const int dimDomain = Base::dimDomain;
  static const int dimRange = Base::dimRange;

  typedef typename Base::DomainType DomainType;
  typedef typename Base::RangeType RangeType;

  std::string gridFile ( const std::string &path ) const
  {
    std::ostringstream dgfFileName;
    dgfFileName << path << "/unitcube" << dimDomain << ".dgf";
    return dgfFileName.str();
  }

  /** \copydoc ProblemData::initial */
  RangeType initial ( const DomainType &x ) const
  {
    return sin( 2 * M_PI * (x*x) );
  }
};

/** \class InitialModel
 *  \brief description of a problem for evaluating initial data
 *
 *  \tparam  dimD  dimension of the domain
 */
template< int dimD >
struct InitialModel
{
  typedef ProblemData< dimD, 1 > Problem;

  static const int dimDomain = Problem::dimDomain;
  static const int dimRange  = Problem::dimRange;

  typedef typename Problem::DomainType DomainType;
  typedef typename Problem::RangeType  RangeType;

  /** \brief constructor
   *
   *  \param[in]  problem  number selecting the problem to be used
   *
   *  \note The following problems have been implemented:
   *  - <em>problem = 1</em>: SinData.
   */
  InitialModel ( int problem )
  {
    switch( problem )
    {
    case 1:
      problem_ = new SinData< dimDomain >();
      break;

    default:
      std::cerr << "Problem not defined - using problem 1!" << std::endl;
      problem_ = new SinData< dimDomain >();
    }
  }

  /** \brief destructor deleting the problem */
  ~InitialModel ()
  {
    delete problem_;
  }

  /** \brief obtain problem data */
  const Problem &problem () const
  {
    return *problem_;
  }

private:
  Problem *problem_;
};
#endif // PROBLEM_HH
