#ifndef DUNE_FEM_SCHOOL_FUNCTION_HH
#define DUNE_FEM_SCHOOL_FUNCTION_HH

/** standard headers **/
#include<iostream>

/** dune headers **/
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

/************************/
/** ContinuousFunction **/
/************************/

/** ContinuousFunction **/
template< int dim, class Impl >
struct ContinuousFunction
{
  /** export dimension of domain **/
  static const int dimension = dim;

  /** single coordinate type **/
  typedef double ctype;

  /** argument type **/
  typedef Dune::FieldVector< ctype, dimension > DomainType;

  /** range type **/
  typedef Dune::FieldVector< ctype, dimension > RangeType;

  /** evaluate **/
  void evaluate ( const DomainType & x, RangeType & y ) const
  {
    asImp().evaluate( x, y );
  }

protected:
  /** constructor **/
  ContinuousFunction () { }

  /** copy constructor **/
  ContinuousFunction ( const ContinuousFunction & ) { }

  /** assignment operator **/
  const ContinuousFunction & operator () ( const ContinuousFunction & ) { return *this; }

  /** cast to implementation **/
  const Impl & asImp () const { return static_cast< const Impl & > (*this); }

};

/****************************/
/** DifferentiableFunction **/
/****************************/

template< int dim, class Impl >
class  DifferentiableFunction
: public ContinuousFunction< dim, Impl >
{
  /** type of base class **/
  typedef ContinuousFunction< dim, Impl > BaseType;

public:
  /** type of jacobian **/
  typedef Dune::FieldMatrix< typename BaseType::ctype, BaseType::dimension, BaseType::dimension > JacobianRangeType;

  /** jacobian **/
  void jacobian ( const typename  BaseType::DomainType & x, JacobianRangeType & j ) const
  {
    asImp().jacobian( x, j );
  }

protected:
  using BaseType::asImp;
};

/******************/
/* LinearFunction */
/******************/

template< int dim, class Impl >
class  LinearFunction
: public DifferentiableFunction< dim, Impl > { };

#endif
