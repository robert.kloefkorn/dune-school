#!/bin/bash
OLD_DIR=`pwd`


RESBRANCH="-b v2019.04"
git clone $RESBRANCH https://github.com/OPM/ResInsight.git
cd ResInsight
mkdir build
cd build
cmake ../ -DRESINSIGHT_BUILD_WITH_QT5=ON -DRESINSIGHT_ENABLE_PROTOTYPE_FEATURE_SOURING=OFF
make -j4

cd $OLDDIR
