#ifndef PIECEWISEFUNCTION_HH
#define PIECEWISEFUNCTION_HH

#include <vector>
#include <fstream>

#include <dune/grid/io/file/vtk/vtksequencewriter.hh>
#include <dune/grid/common/datahandleif.hh>

// PiecewiseFunction
// ----------
/** \class PiecewiseFunction
 *  \brief a piecewise constant function
 *
 *  \tparam  View   grid view on which the function is defined
 *  \tparam  Range  type for the range vector of the function
 *                  This class must have a vector like structure, i.e.,
 *                  a double operator[](int) method.
 *                  Furthermore we expect an axpy method on the class.
 */
template< class View, class Range >
class PiecewiseFunction
{
  typedef PiecewiseFunction< View, Range > This;

public:
  typedef View GridView;
  typedef Range RangeType;

  static const unsigned int order = 0;

  typedef std::vector< RangeType > VectorType;

  typedef typename GridView::template Codim< 0 >::Entity Entity;
  typedef typename GridView::template Codim< 0 >::Geometry Geometry;

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
private:
  struct CommDataHandle;
/*********************************************************/

public:
  /** \brief constructor
   *
   *  \param[in]  gridView  grid view the function lives on
   */
  PiecewiseFunction( const GridView &gridView )
  : gridView_( gridView ),
    dof_( gridView.indexSet().size( 0 ) )
  {}

  const RangeType &operator[] ( const size_t &index ) const
  {
    return dof_[ index ];
  }

  RangeType &operator[] ( const size_t &index )
  {
    return dof_[ index ];
  }

  /** \brief access the DoFs on one entity (of codimension 0)
   *
   *  \param[in]  entity  entity whose DoFs to access
   *
   *  \returns a reference to the DoFs of the entity
   */
  const RangeType &operator[] ( const Entity &entity ) const;

  /** \brief access the DoFs on one entity (of codimension 0)
   *
   *  \param[in]  entity  entity whose DoFs to access
   *
   *  \returns a reference to the DoFs of the entity
   */
  RangeType &operator[] ( const Entity &entity );

  /** \brief obtain the size of the dof vector;
             might be larger that number of elements in grid view. */
  size_t size() const
  {
    return dof_.size();
  }

  /** \brief obtain the grid view, this function lives on */
  const GridView &gridView () const
  {
    return gridView_;
  }

  /** \brief method to initialize data
   *  \param problemData instance of a class having a method initial taking
   *         points \c x in the global coordinate system
   */
  template <class ProblemData>
  void initialize ( const ProblemData &problemData );

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief add a multiple of another function to this one
   *
   *  The classic BLAS level 1 function. In pseudo code it does the following:
   *  \code
   *  (*this) += lambda * other
   *  \endcode
   *
   *  \param[in]  lambda  factor to multiply the other function by
   *  \param[in]  other   function to add to this one
   */
  void axpy ( const double &lambda, const This &other );

  /** \brief set this function to zero */
  void clear ();
/*********************************************************/

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief copy interior cell data to overlap of ghost cells */
  void communicate ();
/*********************************************************/

private:
  GridView gridView_;
  /* storage for dofs */
  VectorType dof_;
};

template< class View, class Range >
inline const typename PiecewiseFunction< View, Range >::RangeType &
PiecewiseFunction< View, Range >::operator[] ( const Entity &entity ) const
{
  assert( gridView().indexSet().contains( entity ) );
  return dof_[ gridView().indexSet().index( entity ) ];
}

template< class View, class Range >
inline typename PiecewiseFunction< View, Range >::RangeType &
PiecewiseFunction< View, Range >::operator[] ( const Entity &entity )
{
  assert( gridView().indexSet().contains( entity ) );
  return dof_[ gridView().indexSet().index( entity ) ];
}

template< class View, class Range >
template< class ProblemData >
inline void
PiecewiseFunction< View, Range >::initialize ( const ProblemData &problemData )
{
  /* first we extract the dimensions of the grid
  static const int dim = GridView::dimension;
  static const int dimworld = GridView::dimensionworld;
  */

  /* get type of iterator over leaf entities of codimension zero */
  typedef typename GridView::template Codim< 0 >::Iterator Iterator;

  /* types of entity and geometry */
  typedef typename GridView::template Codim< 0 >::Entity Entity;
  typedef typename GridView::template Codim< 0 >::Geometry Geometry;

  /* types of vectors */
  typedef typename Geometry::GlobalCoordinate GlobalType;

  // loop over all entities
  const Iterator end = gridView().template end< 0 >();
  for( Iterator it = gridView().template begin< 0 >(); it != end; ++it )
  {
    const Entity &entity = *it;
    const Geometry &geometry = entity.geometry();
    // get barycenter (can be obtained also by geometry.center())
    GlobalType baryCenter = geometry.corner( 0 );
    const int corners = geometry.corners();
    for( int i = 1; i < corners; ++i )
      baryCenter += geometry.corner( i );
    baryCenter /= corners;
    (*this)[ entity ] = problemData.initial( baryCenter );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  // copy data to ghosts
  communicate();
/*********************************************************/

}

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
template< class View, class Range >
inline void
PiecewiseFunction< View, Range >::axpy ( const double &lambda, const This &other )
{
  const size_t size = dof_.size();
  for( size_t i = 0; i < size; ++i )
    (*this)[ i ].axpy( lambda, other[ i ] );
}

template< class View, class Range >
inline void PiecewiseFunction< View, Range >::clear ()
{
  const typename VectorType::iterator end = dof_.end();
  for( typename VectorType::iterator it = dof_.begin(); it != end; ++it )
    *it = RangeType( 0 );
}
/*********************************************************/

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
template< class View, class Range >
inline void PiecewiseFunction< View, Range >::communicate ()
{
  const Dune::InterfaceType interface = Dune::InteriorBorder_All_Interface;
  const Dune::CommunicationDirection direction = Dune::ForwardCommunication;

  CommDataHandle handle( *this );
  gridView().communicate( handle, interface, direction );
}
/*********************************************************/

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
// PiecewiseFunction::CommDataHandle
// ----------------------
/** \brief the communication data handle for the PiecewiseFunction class
 */
template< class View, class Range >
struct PiecewiseFunction< View, Range >::CommDataHandle
: public Dune::CommDataHandleIF< CommDataHandle, Range >
{
  // type of communicated data (i.e. double)
  typedef RangeType DataType;

  //! constructor taking a PiecewiseFunction instance
  CommDataHandle ( PiecewiseFunction< View, Range > &data )
  : data_( data )
  {}

  /****************************************************************
   *** TODO: implement the data handle                          ***
   ****************************************************************/
  //! see documentation in Dune::CommDataHandleIF
  bool contains ( int dim, int codim ) const
  {
    // TODO
    return false;
  }

  //! see documentation in Dune::CommDataHandleIF
  bool fixedsize ( int dim, int codim ) const
  {
    // TODO
    return true;
  }

  //! see documentation in Dune::CommDataHandleIF
  template< class E >
  size_t size ( const E &e ) const
  {
    // TODO
    return 0;
  }

  //! see documentation in Dune::CommDataHandleIF (method for general entities)
  template< class Buffer, class E >
  void gather ( Buffer &buffer, const E &entity ) const
  {
    //TODO
  }

  //! see documentation in Dune::CommDataHandleIF (method for general entities)
  template< class Buffer, class E >
  void scatter ( Buffer &buffer, const E &entity, size_t n)
  {
    // TODO
  }
private:
  // data vector
  PiecewiseFunction< View, Range > &data_;
};
/*********************************************************/

template< class Data >
struct VTKData;

/**
 * \brief a class for vtk output of a PiecewiseFunction instance
 */
template< class GridView, int dimRange >
struct VTKData< PiecewiseFunction< GridView, Dune::FieldVector<double,dimRange> > >
: public Dune::VTKWriter< GridView >::VTKFunction
{
  typedef PiecewiseFunction< GridView, Dune::FieldVector<double,dimRange> > Data;
  typedef VTKData< Data > This;

  static const int dim = GridView::dimension;
  typedef typename GridView::template Codim< 0 >::Entity Entity;
  typedef Dune::FieldVector< double, dim > DomainType;

  //! number of components for scalar use 1 and for vector 2 or 3
  int ncomps () const
  {
    return 1;
  }

  //! evaluate function (comp<ncomps) on entity for local coordinate xi
  double evaluate ( int comp, const Entity &e, const DomainType &xi ) const
  {
    int index = data_.gridView().indexSet().index(e);
    return data_[ index ][ comp_ ];
  }

  //! name for this function
  std::string name () const
  {
    std::stringstream ret;
    ret << name_ << "_" << comp_;
    return ret.str();
  }

  //! add a PiecewiseFunction to the VTKWriter
  static void addTo ( const Data &data, Dune::VTKSequenceWriter< GridView > &vtkWriter )
  {
    /* the vtk-Writer class takes ownership of the function added - VTKData
     * is merely a wrapper for the Data class */
    for( int i = 0; i < dimRange; ++i )
    {
      std::shared_ptr< This > ptr( new This( data, i, "data" ) );
      vtkWriter.addCellData( ptr );
    }
  }
  //! add a PiecewiseFunction to the VTKWriter
  static void addTo ( const Data &data, const std::string &name, Dune::VTKSequenceWriter< GridView > &vtkWriter )
  {
    /* the vtk-Writer class takes ownership of the function added - VTKData
     * is merely a wrapper for the Data class */
    for( int i = 0; i < dimRange; ++i )
    {
      std::shared_ptr< This > ptr( new This( data, i, name ) );
      vtkWriter.addCellData( ptr );
    }
  }

private:
  VTKData ( const Data &data, unsigned int comp, const std::string &name )
  : data_( data ),
    comp_( comp ),
    name_(name)
  {}
  const Data &data_;
  unsigned int comp_;
  const std::string name_;
};

#endif // #ifndef PIECEWISEFUNCTION_HH
