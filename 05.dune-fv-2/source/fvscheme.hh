/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
#ifndef FVSCHEME_HH
#define FVSCHEME_HH

#include <limits>
#include <dune/common/fvector.hh>

#include <dune/grid/common/gridenums.hh>

#include <dune/fem/misc/boundaryidprovider.hh>

// FiniteVolumeScheme
// ------------------
/** \class FiniteVolumeScheme
 *  \brief the implementation of the finite volume scheme
 *
 *  \tparam  V    type of vector modelling a piecewise constant function
 *  \tparam  Model  discretization of the Model.
 *                  This template class must provide
 *                  the following types and methods:
 *  \code
      typedef ... RangeType;
      const ProblemData &problem () const;
      double numericalFlux ( const DomainType &normal,
                             const double time,
                             const DomainType &xGlobal,
                             const RangeType &uLeft,
                             const RangeType &uRight,
                             RangeType &flux ) const;
      double boundaryFlux ( const int bndId,
                            const DomainType &normal,
                            const double time,
                            const DomainType &xGlobal,
                            const RangeType& uLeft,
                            RangeType &flux ) const;
  * \endcode
  */
template< class V, class Model >
struct FiniteVolumeScheme
{
  // first we extract some types
  typedef V Vector;
  typedef typename Vector::GridView GridView;
  typedef typename GridView::Grid Grid;
  static const int dim = GridView::dimension;
  static const int dimworld = GridView::dimensionworld;
  static const int dimRange = Model::dimRange;
  typedef typename Grid::ctype ctype;

  // types of codim zero entity iterator and geometry
  typedef typename GridView::template Codim< 0 >::Iterator  Iterator;
  typedef typename Iterator::Entity                         Entity;
  typedef typename Entity::Geometry                         Geometry;

  /****************************************************************
   *** TODO: Obtain required types:                             ***
   ***       1) IntersectionIterator and Intersection           ***
   ***       2) IntersectionGeometry                            ***
   ****************************************************************/

  // types of vectors
  typedef Dune::FieldVector< ctype, dim-1 >      FaceDomainType;
  typedef Dune::FieldVector< ctype, dim >        DomainType;
  typedef Dune::FieldVector< ctype, dimworld >   GlobalType;
  typedef typename Model::RangeType              RangeType;

public:
  /** \brief constructor
   *
   *  \param[in]  gridView  gridView to operate on
   *  \param[in]  model       discretization of the Model
   */
  FiniteVolumeScheme ( const GridView &gridView, const Model &model )
  : gridView_( gridView )
    , model_( model )
  {}

  /** \brief compute the update vector for one time step
   *
   *  \param[in]   time      current time
   *  \param[in]   solution  solution at time <tt>time</tt>
   *                         (arbitrary type with operator[](const Entity&) operator)
   *  \param[out]  update    result of the flux computation
   *
   *  \returns maximal time step
   */
  template <class Arg>
  double
  operator() ( const double time, const Arg &solution, Vector &update ) const;

	template <class Intersection>
  int boundaryId( const Intersection& intersection ) const
  {
    return Dune::Fem::BoundaryIdProvider< Grid >::boundaryId( intersection );
  }

  /** \brief obtain the grid view for this scheme
   *
   *  \returns the grid view
   */
  const GridView &gridView () const
  {
    return gridView_;
  }

private:
  const GridView gridView_;
  const Model &model_;
}; // end FiniteVolumeScheme

// Implementation of FiniteVolumeScheme
// ------------------------------------

template< class V, class Model >
template< class Arg >
inline double FiniteVolumeScheme< V, Model >
  ::operator() ( const double time, const Arg &solution, Vector &update ) const
{
  // set update to zero
  update.clear();

  // time step size (using std:min(.,dt) so set to maximum)
  double dt = std::numeric_limits<double>::infinity();

  // compute update vector and optimum dt in one grid traversal
  const Iterator endit = gridView().template end< 0 >();
  for( Iterator it = gridView().template begin< 0 >(); it != endit; ++it )
  {
    // get entity and geometry
    const Entity &entity = *it;
    const Geometry &geo = entity.geometry();

    // estimate for wave speed
    double waveSpeed = 0.0;

    // cell volume
    const double enVolume = geo.volume();

    // 1 over cell volume
    const double enVolume_1 = 1.0/enVolume;

    /****************************************************************
     *** TODO: Update on entity                                   ***
     ****************************************************************/
  } // end grid traversal

  // return time step
  return  dt;
}

#endif // #ifndef FVSCHEME_HH
/*********************************************************/

