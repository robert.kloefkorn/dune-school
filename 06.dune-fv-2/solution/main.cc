#include <config.h>
/** \defgroup fv1 A finite-volume method for systems of conservation laws
    \ingroup FV
 **/

/** standard headers **/
#include <iostream>
#include <dune/common/version.hh>

/** dune (mpi, field-vector and grid type for dgf) **/
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/fvector.hh>

typedef Dune::GridSelector::GridType Grid;

/** numerical scheme **/
#include "piecewisefunction.hh"

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
#include "fvscheme.hh"

/*********************************************************/

/** pde and problem description **/
#include "problem.hh"

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
#include "problem-transport.hh"
#include "problem-euler.hh"

/** type of pde to solve **/
typedef TransportModel< Grid::dimensionworld > ModelType;
//typedef EulerModel< Grid::dimensionworld > ModelType;

/*********************************************************/

// method
// ------
void method ( const ModelType &model, int startLevel )
{
  /* Grid construction ... */
  std::string name = model.problem().gridFile( "../data" );
  Dune::GridPtr<Grid> gridPtr(name);

  Grid &grid = *gridPtr;
  grid.loadBalance();

  /* ... some global refinement steps */
  std::cout << "globalRefine: " << startLevel << std::endl;

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  grid.globalRefine( startLevel );

  /* get view to leaf grid */
  typedef Grid::LeafGridView GridView;
  GridView gridView = grid.leafGridView();
/*********************************************************/

  /* construct data vector for solution */
  typedef PiecewiseFunction< GridView, Dune::FieldVector< double, ModelType::dimRange > > DataType;
  DataType solution( gridView );
  /* initialize data */
  solution.initialize( model.problem() );

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /* create finite volume and ODE solver */
  typedef FiniteVolumeScheme< DataType, ModelType > FVScheme;
  FVScheme scheme( gridView, model );
/*********************************************************/

  /* create VTK writer for data sequqnce */
  Dune::VTKSequenceWriter< GridView > vtkOut( gridView, "solution", "../output", ".", Dune::VTK::nonconforming );
  VTKData< DataType >::addTo( solution, vtkOut );

  /* output the initial grid and the solution */
  vtkOut.write( 0.0 );

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /* prepare for time stepping scheme */
  /* final time for simulation */
  const double endTime = model.problem().endTime();
  /* interval for saving data */
  const double saveInterval = model.problem().saveInterval();
  /* first point where data is saved */
  double saveStep = saveInterval;
  /* cfl number */
  double cfl = 0.9;
  /* vector to store update */
  DataType update( gridView );

  /* now do the time stepping */
  unsigned int step = 0;
  double time = 0.0;
  while ( time < endTime )
  {
    // apply the spacial operator
    double dt = scheme( time, solution, update );
    // multiply time step by CFL number
    dt *= cfl;

    // minimize time step over all processes
    dt = solution.gridView().comm().min( dt );
    // communicate update
    update.communicate();

    // update solution
    solution.axpy( dt, update );

    /* augment time */
    time += dt;
    ++step;
    /* check if data should be written */
    if( 1 || time >= saveStep )
    {
      /* visualize with VTK */
      vtkOut.write( time );
      /* set saveStep for next save point */
      saveStep += saveInterval;
    }
    /* print info about time, timestep size and counter */
    if (step % 100 == 0)
    {
      std::cout << "elements = " << gridView.indexSet().size( 0 );
      std::cout << "   maxLevel = " << grid.maxLevel();
      std::cout << "   step = " << step;
      std::cout << "   time = " << time;
      std::cout << "   dt = " << dt;
      std::cout << std::endl;
    }
  }
  /* output final result */
  vtkOut.write( time );
/*********************************************************/

}
/***************************************************
 ** main program with parameters:                 **
 ** 1) number of problem to use (initial data...) **
 ** 2) number of global refinement steps          **
 ** 3) maximal level to use during refinement     **
 ***************************************************/
int main ( int argc , char **argv )
try
{
  /* initialize MPI, finalize is done automatically on exit */
  Dune::MPIHelper &mpi = Dune::MPIHelper::instance( argc, argv );

  if( argc < 2 )
  {
    /* display usage */
    if( mpi.rank() == 0 )
      std::cout << "Usage: " << argv[ 0 ] << " [problem-nr] [startLevel]" << std::endl;
    return 0;
  }
  /* create problem */
  ModelType model(atoi(argv[1]));

  /* get level to use for computationa */
  const int startLevel = (argc > 2 ? atoi( argv[ 2 ] ) : 0);
  method( model, startLevel );

  /* done */
  return 0;
}
catch( const std::exception &e )
{
  std::cout << "STL ERROR: " << e.what() << std::endl;
  return 1;
}
#if ! DUNE_VERSION_NEWER(DUNE_COMMON, 2, 5)
catch( const Dune::Exception &e )
{
  std::cout << "DUNE ERROR: " << e << std::endl;
  return 1;
}
#endif
catch( ... )
{
  std::cout << "Unknown ERROR" << std::endl;
  return 1;
}
